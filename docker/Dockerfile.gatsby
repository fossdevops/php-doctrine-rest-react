# Docker image for building a basic gatsby (static react) site

ARG NODE_IMAGE_VERSION=${NODE_IMAGE_VERSION:-11-alpine}
FROM node:${NODE_IMAGE_VERSION}
#
#  Authorship
#
MAINTAINER geneerik@fossdevops.com

#TODO: other docker metadata

ARG USERNAME=${USERNAME:-apache}
ARG GROUPNAME=${USERNAME:-${USERNAME}}
ARG CREATE_GROUP=${CREATE_GROUP:-true}
ARG CREATE_USER=${CREATE_USER:-true}
ARG UID=${UID:-48}
ARG GID=${GID:-48}
# Need a place to write composer and npm settings junk
ARG USER_HOME_DIR=${USER_HOME_DIR:-/home/${USERNAME}}
ARG PROJECT_ROOT=${PROJECT_ROOT:-/app}
ARG PROJECT_SRC=${PROJECT_ROOT}/src
ARG PROJECT_PUBLIC=${PROJECT_ROOT}/public
ARG GATSBY_TEMPLATE_URI=${GATSBY_TEMPLATE_URI:-https://gitlab.com/fossdevops/gatsby-starter}

# Install needed packages for build project
# get gatsby (includes all the react stuff)
# we install this globally so we dont polute the application source
# create a user for the web app
# Create the site root and give ownership to the site user
RUN apk add --no-cache git && \
    apk add --no-cache --virtual .gyp python make g++ autoconf gettext automake libtool file pkgconf nasm && \
    npm install -g gatsby-cli && \
    (if [ 'true' = "${CREATE_GROUP}" ]; then \
      addgroup -g "${GID}" -S "${GROUPNAME}"; \
    fi) && \
    (if [ 'true' = "${CREATE_USER}" ]; then \
      adduser -h "${USER_HOME_DIR}" -g 'symfony api' -s /sbin/nologin \
              -u "${UID}" -G "${GROUPNAME}" -S -D \
              -H -k /dev/null "${USERNAME}"; \
    fi) && \
    mkdir -p "${USER_HOME_DIR}" && \
    chown -R "${UID}:${GID}" "${USER_HOME_DIR}" && \
    mkdir -p "${PROJECT_ROOT}" && \
    chown -R "${UID}:${GID}" "${PROJECT_ROOT}"

# switch to the webapp user for site install
USER ${USERNAME}

# Switch to the project root directory
WORKDIR ${PROJECT_ROOT}
COPY template-commit-short /version
# Deploy the project template, patch it, and build it
# Doing this all in one shot as kaniko has issue with layering massive numbers of files
# export PROJECT_ROOT=/app && export PROJECT_SRC=/app/src && export PROJECT_PUBLIC=/app/public && export GATSBY_TEMPLATE_URI=https://gitlab.com/fossdevops/gatsby-starter
RUN mkdir -p "${PROJECT_SRC}" && \
    gatsby telemetry --disable && \
    gatsby new "${PROJECT_SRC}" "${GATSBY_TEMPLATE_URI}" && \
    cd "${PROJECT_SRC}" && \
    gatsby build && \
    mv "${PROJECT_SRC}/public" "${PROJECT_PUBLIC}" && \
    cd "${PROJECT_ROOT}" && \
    rm -rf "${PROJECT_SRC}"
# REALLY REALLY; make sure the src dir is GONE (seems to be stubbornly not geting removed some times)
RUN ls -la "${PROJECT_SRC}" && exit 1 || echo "${PROJECT_SRC} removed successfully"
