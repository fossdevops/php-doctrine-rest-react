#!/bin/sh

set -euf
(set -o pipefail || true) > /dev/null
CALLED_SCRIPT_NAME=${0}
trap '[ $? -eq 0 ] && exit 0 || echo '"'"'Exception.Capture: '"${CALLED_SCRIPT_NAME}"': '"'\"\${LINENO}\"" EXIT

if [ ! -z "${BASH_SOURCE:-}" ]; then
  CALLED_SCRIPT_NAME=${BASH_SOURCE[0]}
##ksh
#else
#  #CALLED_SCRIPT_NAME=${(%):-%N}
#TODO: osx/bsd
fi

SCRIPT_DIR="$(cd "$(dirname "${CALLED_SCRIPT_NAME}")" && pwd)"
SCRIPT_NAME="$(basename "${CALLED_SCRIPT_NAME}")"
SCRIPT_PATH="${SCRIPT_DIR}/${SCRIPT_NAME}"

trap - EXIT
trap '[ $? -eq 0 ] && exit 0 || echo '"'"'ERROR: '"${SCRIPT_PATH}"': '"'\"\${LINENO}\"" EXIT

SITE_ROOT="${SITE_ROOT:-/var/www-symfony-api}"
OUTPUT_DIR="${OUTPUT_DIR:-/output}"

if [ 'true' = "${INSTALL_CPIO:-true}" ]; then
  apk add cpio
fi

cd "${SITE_ROOT}" && (find . | cpio -pamvVd "${OUTPUT_DIR}")