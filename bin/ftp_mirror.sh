#!/bin/sh

set -euf
set +e
(set -o pipefail || true) 2>/dev/null
set -e
CALLED_SCRIPT_NAME=${0}
trap '[ $? -eq 0 ] && exit 0 || echo '"'"'Exception.Capture: '"${CALLED_SCRIPT_NAME}"': '"'\"\${LINENO}\"" EXIT

if [ ! -z "${BASH_SOURCE:-}" ]; then
  CALLED_SCRIPT_NAME=${BASH_SOURCE[0]}
##ksh
#else
#  #CALLED_SCRIPT_NAME=${(%):-%N}
#TODO: osx/bsd
fi

SCRIPT_DIR="$(cd "$(dirname "${CALLED_SCRIPT_NAME}")" && pwd)"
SCRIPT_NAME="$(basename "${CALLED_SCRIPT_NAME}")"
SCRIPT_PATH="${SCRIPT_DIR}/${SCRIPT_NAME}"

trap - EXIT
trap '[ $? -eq 0 ] && exit 0 || echo '"'"'ERROR: '"${SCRIPT_PATH}"': '"'\"\${LINENO}\"" EXIT

PUBLIC_DIR=${PUBLIC_DIR:-public}
DNS_ORDER=${DNS_ORDER:-inet6 inet}

lftp -c "set ftp:list-options -a;
set xfer:clobber true;
set dns:order \"${DNS_ORDER}\";
set net:timeout 30;
set net:max-retries 3;
set net:reconnect-interval-multiplier 1;
set net:reconnect-interval-base 5;
open ${FTP_SITE}; 
cd /;
dir;
mirror --dereference --max-errors=0 --reverse --delete --no-symlinks --use-cache --verbose --allow-chown --allow-suid --no-umask --parallel=1 --exclude var/log --exclude env --exclude ${PUBLIC_DIR}/.htaccess --exclude .env.local --exclude .gitlab-ci.yml --exclude ftp_mirror.sh --exclude .git --exclude public_react"
